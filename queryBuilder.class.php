<?php

/**
 * This is a simple class aimed at making the construction of SQL queries that are more robust and reliable.
 *
 * EXAMPLE USAGE
 * Super simple example:
 * $builder = new queryBuilder($dbPDO); // where $dbPDO is an instance of the $dbPDO class or one of its children
 * $query = $builder->build('SELECT', 'fooTable'); // returns 'SELECT * FROM fooTable'
 *
 * Sample simple example using build from array:
 * $builder = new queryBuilder($dbPDO); // where $dbPDO is an instance of the $dbPDO class or one of its children
 * $opts['type'] = 'SELECT';
 * $opts['table'] = 'fooTable'
 * $query = $builder->buildFromArray($opts);  // returns 'SELECT * FROM fooTable'
 *
 * More complex example using all parameters:
 * $builder = new queryBuilder($dbPDO); // where $dbPDO is an instance of the $dbPDO class or one of its children
 * $fields = array('this', 'that', 'otherThing');
 * $query = $builder->build('SELECT', 'fooTable', $fields, null, 'otherThing ASC', 100);
 *
 * The above becomes
 * 'SELECT this, that, otherThing FROM fooTable ORDER BY otherThing ASC LIMIT 100'
 *
 * Same example using build from array
 * $opts['type'] = 'SELECT';
 * $opts['table'] = 'fooTable';
 * $opts['fields'] = array('this', 'that', 'otherThing');
 * $opts['order'] = 'otherThing ASC';
 * $opts['limit'] = 100;
 * $query = $builder->buildFromArray($opts);
 */
class queryBuilder
{

    private $db;

    /**
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
    }


    /**
     * @param        $type
     * @param        $tables
     * @param string $fields
     * @param null   $conditions
     * @param null   $order
     * @param null   $limit
     *
     * @return mixed|string
     */
    public function build($type, $tables, $fields = '*', $conditions = null, $order = null, $limit = null)
    {
        $output = $this->type($type);
        $output .= $this->tables($tables);
        $output .= $this->fields($fields);
        $output .= $this->conditions($conditions);
        $output .= $this->order($order);
        $output .= $this->limit($limit);

        return $output;
    }

    /**
     * @param $array
     *
     * @return string|void
     */
    public function buildFromArray($array)
    {
        $array = $this->validateBuildArray($array);

        return $this->build($array['type'], $array['tables'], $array['fields'], $array['conditions'], $array['order'], $array['limit']);
    }

    /**
     * Make sure all if the expected keys are set (even if that setting ends up being null)
     * and make sure nothing extra is included (because it won't be used anyway)
     *
     * @param $array
     *
     * @return mixed
     */
    private function validateBuildArray($array)
    {
        $expectedKeys = array('type', 'tables', 'fields', 'conditions', 'order', 'limit');

        foreach ($expectedKeys AS $key) {
            if (!isset($array[$key])) {
                $array[$key] = null;
            }
        }

        foreach ($array AS $key => $val) {
            if (!in_array($key, $expectedKeys)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * @param $array
     *
     * @return string
     */
    private function stringify($array)
    {
        if (!is_array($array)) {
            return Strings::strPad($array);
        } else {
            if (Arrays::arrayIsAssoc($array)) {
                foreach ($array AS $key => $val) {
                    $newArray[] = Strings::strPad($key . "=" . "' . $this->db->EscapeString($val) . '");
                }
                Strings::strPad(implode(',', $newArray));
            } else {
                return Strings::strPad(implode(',', $array));
            }
        }
    }

    /**
     * @param $type
     *
     * @return mixed
     */
    public function type($type)
    {
        return Strings::strPad($type);
    }


    /**
     * @param $arg
     * @param $pre
     *
     * @return string
     */
    public function tables($arg, $pre='FROM')
    {
        return Strings::strPad(sprintf('%s', $pre) . $this->stringify($arg));
    }

    /**
     * @param $arg
     *
     * @return string
     */
    public function fields($arg)
    {
        return Strings::strPad($this->stringify($arg));
    }

    /**
     * @param $arg
     *
     * @return string
     */
    public function conditions($arg)
    {
        return Strings::strPad($this->stringify($arg));
    }

    /**
     * @param $arg
     *
     * @return string
     */
    public function order($arg)
    {
        return Strings::strPad('ORDER BY' . $this->stringify($arg));
    }

    /**
     * @param $arg
     *
     * @return string
     */
    public function limit($arg)
    {
        return Strings::strPad('LIMIT' . $this->stringify($arg));
    }

}