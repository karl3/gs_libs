var class_input_filter =
[
    [ "__construct", "class_input_filter.html#ac48b3a07e4a3516c1d85ae3900cf3fb9", null ],
    [ "decode", "class_input_filter.html#a8c18f57237b969231418c2d66ecd125d", null ],
    [ "escapeString", "class_input_filter.html#afadf58dcf9a66f9bf52ee8d4a19db97b", null ],
    [ "filterAttr", "class_input_filter.html#aae4567b940a1791eaf23770c4aa4376e", null ],
    [ "filterTags", "class_input_filter.html#a533c70ae138d5d14952ff868a168400d", null ],
    [ "process", "class_input_filter.html#ac31900343e78d4b54e911d5af3c0b5e0", null ],
    [ "quoteSmart", "class_input_filter.html#a93797cf4a5128b15af79cb1c41553f32", null ],
    [ "remove", "class_input_filter.html#a591a2b5e435a9d6eabe460819c41159c", null ],
    [ "safeSQL", "class_input_filter.html#a178f6ea15a691d60ebdcbcd2b7dbc743", null ],
    [ "$attrArray", "class_input_filter.html#a39fb95773386c0379371b2520dd8cd8d", null ],
    [ "$attrBlacklist", "class_input_filter.html#a5d1cf3b7473a284ddbb8573cf52a2718", null ],
    [ "$attrMethod", "class_input_filter.html#a3d010c4cf00e557c98fdfd6376ff85b4", null ],
    [ "$tagBlacklist", "class_input_filter.html#a729b176d3d97e4d8961e15fe622ea58e", null ],
    [ "$tagsArray", "class_input_filter.html#ae7f8f42c50985c03196222b4f1539ee0", null ],
    [ "$tagsMethod", "class_input_filter.html#a8045801eda1e935339329800ec06bfba", null ],
    [ "$xssAuto", "class_input_filter.html#aebb1642f22abcf61c4a863789e36bbb1", null ]
];