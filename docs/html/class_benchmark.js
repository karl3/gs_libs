var class_benchmark =
[
    [ "__construct", "class_benchmark.html#af7d99cdaac06dc7b47d681b4cfbb06bc", null ],
    [ "__destruct", "class_benchmark.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "display", "class_benchmark.html#aa836be07a8d7f803a8f6acc965acf2a3", null ],
    [ "logtoDB", "class_benchmark.html#ae0a10b5f75fa87d91e692d4f276ac7ae", null ],
    [ "start", "class_benchmark.html#af8fa59992209e36dccb3eefb0f75531f", null ],
    [ "stop", "class_benchmark.html#a8b6fc76a620d7557d06e9a11a9ffb509", null ],
    [ "$db", "class_benchmark.html#a1fa3127fc82f96b1436d871ef02be319", null ],
    [ "$end_time", "class_benchmark.html#a53df77622d4c20adefa70db294377d7b", null ],
    [ "$execution_time", "class_benchmark.html#a2d8dc53e1ba1a00dd213589f7a5f4377", null ],
    [ "$start_time", "class_benchmark.html#a73a034a7a0fbdd2c93f23fb6c9946ee9", null ]
];