var class_stats =
[
    [ "__construct", "class_stats.html#a9b576568bc17458490e6985a7489c3a9", null ],
    [ "__destruct", "class_stats.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "countDates", "class_stats.html#adf94df76cef17a9a175924473b71446e", null ],
    [ "getFirstDate", "class_stats.html#a025ccf47b1021dba92cb33c15b36e63b", null ],
    [ "getLastDate", "class_stats.html#a4de981ae1f7c64de95d1d98f001b3e5f", null ],
    [ "getRawStats", "class_stats.html#af0592a90f0b14c809dad12c026a18b68", null ],
    [ "getTotalDistinctIPs", "class_stats.html#ab8b13de24fa5d9e50a0d7cf9e9f1af32", null ],
    [ "getTotalDistinctPages", "class_stats.html#aaf9ae81485efae3ce6f902ddac969a45", null ],
    [ "getTotalDistinctSessions", "class_stats.html#aa186bab67dcb150d445b84e6ec52cfd9", null ],
    [ "getTotalDistinctURIs", "class_stats.html#a0c99010e511c7fc19ad59f16179524cb", null ],
    [ "getTotalDistinctUserAgents", "class_stats.html#a85991d88c2f5401b051cc554b04bdec8", null ],
    [ "getTotalDistinctUsers", "class_stats.html#a6c944fec89049927b811bd786a934e96", null ],
    [ "getTotalPageViews", "class_stats.html#a017d22295900a75171f1a3e5b488eb09", null ],
    [ "insertStats", "class_stats.html#aec6efc1f87114d251b6d91be90bc3ddd", null ],
    [ "setIgnore", "class_stats.html#a604a265217a1774947350bca38b39e01", null ],
    [ "$connection", "class_stats.html#a0d9c79b9b86b3f5891c6d3892f12c6a0", null ],
    [ "$current_user", "class_stats.html#a12f5443cc19cb33ae0292013c53a7b19", null ],
    [ "$Ignore", "class_stats.html#aa7c64052a6704d3e4c8d4a17a506fed3", null ],
    [ "$tableName", "class_stats.html#aea06aaf672e3fe5fbf45958ae2782244", null ]
];