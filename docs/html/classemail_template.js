var classemail_template =
[
    [ "__construct", "classemail_template.html#a4d8e4a324efce73096d5af1d4c143fdd", null ],
    [ "__destruct", "classemail_template.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "__get", "classemail_template.html#aa36766cc96e8ead6a654535cd826cc92", null ],
    [ "__set", "classemail_template.html#a1e08cf354600b2e56e93bd4f59636937", null ],
    [ "clearTags", "classemail_template.html#a66517013fef49505b145d9f8bccfa6d3", null ],
    [ "load", "classemail_template.html#aaa4e95f27857ab78defda3e0c0b7039b", null ],
    [ "parse", "classemail_template.html#a38d99acc70a1d8fd8f94455743b2d237", null ],
    [ "send", "classemail_template.html#a7be7ce516e2b409907f824e06957b923", null ],
    [ "setDefaults", "classemail_template.html#ac900283b8780520c328c6457e34177f4", null ],
    [ "setTags", "classemail_template.html#a182cb7b16d045c4aa1cbf6e0cd845933", null ],
    [ "unload", "classemail_template.html#aa13d0535d22492ba710e3419a7b92ac1", null ],
    [ "$path", "classemail_template.html#a0a4baf0b22973c07685c3981f0d17fc4", null ],
    [ "$tags", "classemail_template.html#a475a6a63b85186663d34151bcbd21590", null ],
    [ "$template", "classemail_template.html#aa3e9534005fd516d941f6a5569896e01", null ]
];