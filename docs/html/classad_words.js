var classad_words =
[
    [ "__construct", "classad_words.html#a155afedbdfa01f1c0b813ca97642cdab", null ],
    [ "__destruct", "classad_words.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "deleteAdwordsContent", "classad_words.html#a4895a797b1ef94a475613aeec6ad18a0", null ],
    [ "getAdwordsContentByID", "classad_words.html#a53116b77ed57dc81c0ce4f99102fd73e", null ],
    [ "getAdwordsContentHTML", "classad_words.html#a3049482d3f7c582b1f4b4f0bf3055029", null ],
    [ "getAWReferrals", "classad_words.html#a83cfc1f27fa45ceae5b19ee48a7b69a4", null ],
    [ "getAWReferralsByID", "classad_words.html#a0bdc9669061797f51ced6bf2aaa8b8f2", null ],
    [ "getURLParams", "classad_words.html#ab266e93fa2a60e1040af77722b108409", null ],
    [ "insertAdwordsContent", "classad_words.html#afed5aecd2349d5c4c6f8b0ca0f8ac475", null ],
    [ "insertAWReferral", "classad_words.html#a21aaab50551efd2135c7cba5bb2a1d3c", null ],
    [ "setExpectedGet", "classad_words.html#a9bf83ac28c970180dd0a19c947b228af", null ],
    [ "updateAdwordsContent", "classad_words.html#a9505e1b708f6f8b947af4aac60b6e15a", null ],
    [ "$connection", "classad_words.html#a0d9c79b9b86b3f5891c6d3892f12c6a0", null ],
    [ "$expectedAWGet", "classad_words.html#a0f0c39307713afefee4b098aa9e67e9c", null ],
    [ "$urlParams", "classad_words.html#af8ca0dec661a0a50335054efa40d8ce6", null ]
];