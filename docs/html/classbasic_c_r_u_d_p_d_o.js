var classbasic_c_r_u_d_p_d_o =
[
    [ "__construct", "classbasic_c_r_u_d_p_d_o.html#a7f93adf2a111686240ea50fdd8bd8ebf", null ],
    [ "__destruct", "classbasic_c_r_u_d_p_d_o.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "countItems", "classbasic_c_r_u_d_p_d_o.html#ab308739e3ca8d66719416002fd06fe5d", null ],
    [ "delete", "classbasic_c_r_u_d_p_d_o.html#a0b1faa3905d79b263299154d0e51f408", null ],
    [ "getAll", "classbasic_c_r_u_d_p_d_o.html#a2803d8e3b0a8dcc8f1efc580d4b254fe", null ],
    [ "getByID", "classbasic_c_r_u_d_p_d_o.html#a7eb0e7204cac6c32a450c90a90d611b1", null ],
    [ "insert", "classbasic_c_r_u_d_p_d_o.html#adabf861bb374d51cf007f9b6888303cf", null ],
    [ "SQL2HTMLTable", "classbasic_c_r_u_d_p_d_o.html#a6c556dc28409d01baef0f524ecb66819", null ],
    [ "update", "classbasic_c_r_u_d_p_d_o.html#ab6c22e1f41766b0678cdd7c503baaa1f", null ]
];