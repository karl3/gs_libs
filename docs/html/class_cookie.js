var class_cookie =
[
    [ "Lifetime", "class_cookie.html#ae4b90b216439b5f4fd00c88bd87a26e1", null ],
    [ "OneDay", "class_cookie.html#aa521862df1faf3bae91e826bdd7c4307", null ],
    [ "OneHour", "class_cookie.html#ab0d5d43ef5de3a8b8d246f38d4011644", null ],
    [ "OneYear", "class_cookie.html#ae1a7aa52bbb1caabe1babbfb6b74921c", null ],
    [ "Session", "class_cookie.html#a3880a2d65111dd04ed2762c3516238c4", null ],
    [ "SevenDays", "class_cookie.html#a272654e12377439eb99efbc6cb9493b0", null ],
    [ "SixMonths", "class_cookie.html#a89aba8b2219057356a151cca43412b1d", null ],
    [ "ThirtyDays", "class_cookie.html#ae5e80f88a08010c095ce85a659f5c819", null ]
];