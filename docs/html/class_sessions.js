var class_sessions =
[
    [ "__construct", "class_sessions.html#a8cd53e3840b870683ec955ec89da3abe", null ],
    [ "__destruct", "class_sessions.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "close", "class_sessions.html#aa69c8bf1f1dcf4e72552efff1fe3e87e", null ],
    [ "destroy", "class_sessions.html#a726fa8a4b4b187b9ca32ba427aac8137", null ],
    [ "gc", "class_sessions.html#a14ff7ef4b198ff14884dd8c564264ca3", null ],
    [ "open", "class_sessions.html#a9824b1cfea0c3b5874afd027c4812344", null ],
    [ "read", "class_sessions.html#afa59bebedda70c37b94c2efc35da83f3", null ],
    [ "resetSessionID", "class_sessions.html#a1d183070ef4f11c8899b6dcbed874f40", null ],
    [ "setSessionVars", "class_sessions.html#ae3ae7294fa85454de518e45e1c36fcf6", null ],
    [ "write", "class_sessions.html#a5f277b5f0e4e2154cddc9a3a0d2bf57d", null ]
];