var classip_api =
[
    [ "__construct", "classip_api.html#a68c955b7f95b2bae74fe11c3b1347be5", null ],
    [ "__destruct", "classip_api.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "getIpInfo", "classip_api.html#a686b062a4ea72799099b8151970829de", null ],
    [ "getIpInfoFromDB", "classip_api.html#a649701a5d2cd6760b96955b3929575ca", null ],
    [ "ipExists", "classip_api.html#a9826f5ea66ee734dc7235094c0833c68", null ],
    [ "logToDB", "classip_api.html#a57a840fd314080f4b67086edbfdb995a", null ],
    [ "$apiURL", "classip_api.html#ae55a59ebe41af641d66d9cfd81cf959f", null ],
    [ "$dbTable", "classip_api.html#adfd23e5286c56c7632ecb55887dd2555", null ]
];