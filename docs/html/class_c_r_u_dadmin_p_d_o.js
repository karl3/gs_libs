var class_c_r_u_dadmin_p_d_o =
[
    [ "__construct", "class_c_r_u_dadmin_p_d_o.html#a7f93adf2a111686240ea50fdd8bd8ebf", null ],
    [ "__destruct", "class_c_r_u_dadmin_p_d_o.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "adminTable", "class_c_r_u_dadmin_p_d_o.html#a70c3755733d946abf4348bcdc431dbaf", null ],
    [ "config", "class_c_r_u_dadmin_p_d_o.html#a057f051ca656f78a79ca981dacebc88e", null ],
    [ "delete", "class_c_r_u_dadmin_p_d_o.html#a2f8258add505482d7f00ea26493a5723", null ],
    [ "getAll", "class_c_r_u_dadmin_p_d_o.html#a5bd5a575f9e2633945d1abf324ffbcd4", null ],
    [ "getByID", "class_c_r_u_dadmin_p_d_o.html#a53634d1376ca031c9fb15fc6c414b38d", null ],
    [ "insert", "class_c_r_u_dadmin_p_d_o.html#a2e355627d2f4a96559f022c4ab82df3c", null ],
    [ "lastInsertID", "class_c_r_u_dadmin_p_d_o.html#a342821d7c67c9611bc7be824c5df8464", null ],
    [ "purgeDeleted", "class_c_r_u_dadmin_p_d_o.html#a3f2ceed45deff29e9ddceecacc401b21", null ],
    [ "realDelete", "class_c_r_u_dadmin_p_d_o.html#a62e2e537ded1658daab4890c47e65cac", null ],
    [ "SelectSpecificRecord", "class_c_r_u_dadmin_p_d_o.html#ac591b2f363dc25c0c4fb3908a66fc510", null ],
    [ "unDelete", "class_c_r_u_dadmin_p_d_o.html#a6c93c8a03666211ecc629f533d87fede", null ],
    [ "update", "class_c_r_u_dadmin_p_d_o.html#a1d8c1067487068c15fdf6bf9a1c9cad5", null ],
    [ "$dbTable", "class_c_r_u_dadmin_p_d_o.html#adfd23e5286c56c7632ecb55887dd2555", null ],
    [ "$PKfield", "class_c_r_u_dadmin_p_d_o.html#a8990497e330f7dbddd7c67e496bfa33b", null ]
];