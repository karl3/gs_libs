var classdb_backup =
[
    [ "__construct", "classdb_backup.html#a7f93adf2a111686240ea50fdd8bd8ebf", null ],
    [ "__destruct", "classdb_backup.html#a421831a265621325e1fdd19aace0c758", null ],
    [ "backupTable", "classdb_backup.html#a75da871b294ce5ac4e4e1ab1ae0bc5d4", null ],
    [ "dumpMultipleTables", "classdb_backup.html#a18a10140bf184faeb780ce8493446c55", null ],
    [ "getData", "classdb_backup.html#ac05be54f45477b0eeb08b0693dfa48ac", null ],
    [ "setPath", "classdb_backup.html#a5d101758b135bd36ba8992cc3bb67a24", null ],
    [ "setPostString", "classdb_backup.html#afe408bb74f4a63a1be4f8db787b59453", null ],
    [ "setPreString", "classdb_backup.html#af9ff1acb357a6c171c60643144717694", null ],
    [ "tableColumns", "classdb_backup.html#a0aa0472caa524c75db0b2f9cddc594c5", null ],
    [ "writeFile", "classdb_backup.html#aaa28a36632ff9f6df04cc3bbc13a8118", null ]
];